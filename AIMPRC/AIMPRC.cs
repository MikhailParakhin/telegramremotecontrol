﻿using AIMP.SDK;
using AIMP.SDK.Player;
using AIMP.SDK.Playlist;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AIMPRC
{
    public class PlayListControl
    {
        private readonly IAimpPlaylist _playList;
        private readonly IAimpPlayer _player;

        public List<IAimpPlaylistItem> ItemsList { get; set; } = new List<IAimpPlaylistItem>();

        public PlayListControl(IAimpPlaylist playList, IAimpPlayer player)
        {
            _playList = playList;
            _player = player;

            LoadTracks();
            
            playList.Changed += (sender, type) =>
            {
                if (type.HasFlag(PlaylistNotifyType.AIMP_PLAYLIST_NOTIFY_STATISTICS))
                {
                    RefreshTracks();
                }
            };
        }
        
        public string GetCurrentTrack()
        {
            return GetTrackInfo(_player.CurrentPlaylistItem);
        }

        public void ChangeTrack(string artist, string title)
        {
            if (artist == null && title == null)
            {
                return;
            }

            var item = FindTrack(artist, title);
            if (item != null)
            {
                _player.Play(item);
            }
        }

        private void RefreshTracks()
        {
            ItemsList.Clear();
            LoadTracks();
        }

        private IAimpPlaylistItem FindTrack(string artist, string title)
        {
            RefreshTracks();
            var result = ItemsList.Where(x => x.FileInfo.Title.ToLower().Contains(title.ToLower()));

            if (artist != null)
                result = result.Where(x => x.FileInfo.Artist.ToLower().Contains(artist.ToLower()));

            return result.FirstOrDefault();
        }

        private void LoadTracks()
        {
            IList<string> files;
            //if (CheckResult(_playList.GetFiles(PlaylistGetFilesFlag.All, out files)) == AimpActionResult.OK)
            {
                int count = _playList.GetItemCount();
                for (var i = 0; i < count; i++)
                {
                    var item = _playList.GetItem(i);
                    if (item == null)
                    {
                        continue;
                    }

                    ItemsList.Add(item);
                }
            }
        }

        private AimpActionResult CheckResult(AimpActionResult actionResult)
        {
            if (actionResult != AimpActionResult.OK)
            {
                throw new Exception("ERROR");
            }

            return actionResult;
        }

        private string GetTrackInfo(IAimpPlaylistItem item)
        {
            var trackItem = item.PlaybackIndex.ToString();
            trackItem += " " + item.DisplayText;
            trackItem += " " + TimeSpan.FromSeconds(item.FileInfo.Duration).ToString();

            return item.DisplayText;
        }
    }
}
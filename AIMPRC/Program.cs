﻿using System;
using System.IO;
using System.IO.Pipes;
using System.Linq;
using System.Windows.Forms;
using AIMP.SDK.Playlist;
using AIMP.SDK;
using AIMP.SDK.MenuManager;
using Helpers;

namespace AIMPRC
{
    public class ExtensionPlaylistManagerListener : IAimpExtension, IAimpExtensionPlaylistManagerListener
    {
        public AimpActionResult OnPlaylistActivated(IAimpPlaylist playlist)
        {
            return AimpActionResult.Ok;
        }

        public AimpActionResult OnPlaylistAdded(IAimpPlaylist playlist)
        {
            return AimpActionResult.Ok;
        }
        public AimpActionResult OnPlaylistRemoved(IAimpPlaylist playlist)
        {
            return AimpActionResult.Ok;
        }
    }

    [AimpPlugin("dotnet_demo", "Evgeniy Bogdan", "1", AimpPluginType = AimpPluginType.Addons)]
    public class Program : AimpPlugin
    {
        public override void Initialize()
        {
            IAimpMenuItem demoFormItem;
            if (Player.MenuManager.CreateMenuItem(out demoFormItem) == AimpActionResult.Ok)
            {
                demoFormItem.Name = "Open demo form";
                demoFormItem.Id = "demo_form";
                demoFormItem.Style = AimpMenuItemStyle.CheckBox;
                
                demoFormItem.OnExecute += DemoFormItemOnOnExecute;
                demoFormItem.OnShow += (sender, args) =>
                {
                    MessageBox.Show("asdasd");
                    IAimpPlaylist playlist;
                    Player.PlaylistManager.GetLoadedPlaylist(0, out playlist);
                    var playlistControl = new PlayListControl(playlist, Player);

                    var serverPipe = new NamedPipeServerStream("AimpRcPipe2", PipeDirection.InOut);
                    serverPipe.WaitForConnection();

                    if (!serverPipe.IsConnected)
                    {
                        return;
                    }

                    var helper = new InitializationHelper();

                    Player.TrackChanged += (sender1, args1) =>
                    {
                        var trackView = playlistControl.GetCurrentTrack();
                        helper.SendMessage(trackView);
                    };

                    ChangeTrackFromExternalCommand += (sender2, args2) =>
                    {
                        var argsSplit = args2.Text.Split('-').Select(x => x.Trim()).ToArray();
                        var artist = argsSplit.Length > 1 ? argsSplit[0] : null;
                        var track = argsSplit.Length > 1 ? argsSplit[1] : argsSplit.Length > 0 ? argsSplit[0] : null;
                        playlistControl.ChangeTrack(artist, track);
                    };

                    var pipe = new NamedPipeClientStream(".", "AimpRcPipe2", PipeDirection.In);

                    pipe.Connect();

                    using (var sw = new StreamReader(pipe))
                    {
                        while (true)
                        {
                            string str;
                            if ((str = sw.ReadLine()) != null)
                            {
                                ChangeTrackFromExternalCommand(this, new TextEventArgs() {Text = str});
                            }
                        }
                    }
                };

                Player.MenuManager.Add(ParentMenuType.AIMP_MENUID_COMMON_UTILITIES, demoFormItem);
            }
        }

        private event EventHandler<TextEventArgs> ChangeTrackFromExternalCommand = delegate { };

        private void DemoFormItemOnOnExecute(object sender, EventArgs eventArgs)
        {
            var item = sender as IAimpMenuItem;
            //Logger.Instance.AddInfoMessage($"Event: [Execute] {item.Id}");
        }

        public override void Dispose()
        {
            System.Diagnostics.Debug.WriteLine("Dispose");
            //Player.MenuManager.Delete(_menuItem);
        }

        private class TextEventArgs : EventArgs
        {
            public string Text { get; set; }
        }
    }
}
 
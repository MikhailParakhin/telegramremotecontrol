﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Threading;
using Helpers;
using Application = System.Windows.Application;
using MessageBox = System.Windows.MessageBox;

namespace TelegramRemoteControlUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        private readonly InitializationHelper _helper;

        //TODO https://pikabu.ru/story/otpravlyaem_snimok_s_vebkameryi_v_telegram_pri_obnaruzhenii_dvizheniya_5926078
        public MainWindow()
        {
            _helper = new InitializationHelper(WriteLine, Remind());
            InitializeComponent();
            NotifyIcon = new NotifyIcon
            {
                Icon = new Icon("Icon.ico"),
                Visible = true,
                ContextMenu = new ContextMenu(new[] {new MenuItem("Restart", RestartButton_Click), new MenuItem("Exit", ExitButton_Click),})
            }; //TODO icon from chat
            NotifyIcon.MouseDoubleClick += NotifyIcon_MouseDoubleClick;

            string error;
            _helper.Start(out error);
            if (error != null)
            {
                MessageBox.Show(error);
                Close();
            }
        }
        
        private void WriteLine(string str)
        {
            MessageText = $"{str}\n{MessageText}";
            OnPropertyChanged(MessageText);
        }

        private Action<string> Remind()
        {
            return x =>
            {
                if (Application.Current.Dispatcher.CheckAccess())
                {
                    MessageBox.Show(Application.Current.MainWindow, x, "REMINDER");
                }
                else
                {
                    Application.Current.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() => { MessageBox.Show(Application.Current.MainWindow, x, "REMINDER"); }));
                }
                //TODO foreground
            };
        }

        private NotifyIcon NotifyIcon { get; set; }

        public string MessageText { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string value)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(MessageText)));
        }

        private void NotifyIcon_MouseDoubleClick(object sender, MouseEventArgs mouseEventArgs)
        {
            //TODO unminimizing doesnt work
            WindowState = WindowState.Normal;
            OnStateChanged(new EventArgs());
            Show();
            //Focus();
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            e.Cancel = true;
            WindowState = WindowState.Minimized;
            Hide();
        }

        private void ExitButton_Click(object sender, EventArgs e)
        {
            _helper.Stop();
            NotifyIcon.Visible = false;
            NotifyIcon.Dispose();
            Close();
            Application.Current.Shutdown();
        }

        private void RestartButton_Click(object sender, EventArgs e)
        {
            _helper.RestartServiceOverTime(WriteLine, Remind(), runOnce: true);
        }
    }
}

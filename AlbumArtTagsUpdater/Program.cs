﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TagLib;

namespace AlbumArtTagsUpdater
{
    class Program
    {
        public static void Main(string[] args)
        {
            string[] fileNames;
            try
            {
                fileNames = Directory.GetFiles("D:\\Documents\\Music\\Loseless", "*.flac", SearchOption.AllDirectories);
            }
            catch (Exception)
            {
                return;
            }

            var currentPath = "";
            var pics = new string[] { };
            var bytes = new byte[] { };
            var counter = 0;

            foreach (var fileName in fileNames)
            {
                counter++;
                var newPath = currentPath != fileName.Substring(0, fileName.LastIndexOf("\\"));
                currentPath = newPath ? fileName.Substring(0, fileName.LastIndexOf("\\")) : currentPath;
                pics = newPath ? Directory.GetFiles(currentPath, "*.jpg", SearchOption.TopDirectoryOnly) : pics;

                var flac = new TagLib.Flac.File(fileName);
                if (!flac.Tag.Pictures.Any() && pics.FirstOrDefault() != null)
                {
                    bytes = newPath ? System.IO.File.ReadAllBytes(pics.FirstOrDefault()) : bytes;
                    var asd = new[] { new Pic() { Type = PictureType.FrontCover, MimeType = "image/jpeg", Data = new ByteVector(bytes) } };
                    flac.Tag.Pictures = asd;
                    flac.Save();
                    Console.WriteLine(((int)(100 * counter / fileNames.Count())) + "% - " + fileName);
                }
                else if (!flac.Tag.Pictures.Any() && pics.FirstOrDefault() == null)
                {
                    Console.WriteLine("NOT FOUND - " + fileName);
                }
            }

            Console.ReadKey();
        }

        private class Pic : IPicture
        {
            public string MimeType { get; set; }
            public PictureType Type { get; set; }
            public string Description { get; set; }
            public ByteVector Data { get; set; }
        }
    }
}

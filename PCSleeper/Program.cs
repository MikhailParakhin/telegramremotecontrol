﻿using System;
using System.Threading;
using Helpers;

namespace PCSleeper
{
    public class Program
    {
        public static void Main(string[] args)
        {            
            Console.WriteLine("Enter sleep delay in minutes:");
            var str = Console.ReadLine();
            var minutes = str.ToInt() ?? 1;
            Console.Clear();
            if (str.ToInt() == null)
            {
                Console.WriteLine($"Wrong number entered, sleep after default {minutes} minutes");
            }


            try
            {
                Thread.Sleep(5000);

                using (var brightnessController = new BrightnessController())
                {
                    brightnessController.SetBrightness(10);
                }

                new SoundHelper().SmoothVolumeChange("Монитор", 44, minutes, () => { WindowsOperationsHelper.Sleep().WaitForExit(); });

                using (var brightnessController = new BrightnessController())
                {
                    brightnessController.SetBrightness(90);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadKey();
            }
        }
    }
}

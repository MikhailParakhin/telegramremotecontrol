﻿using System;
using System.IO;
using System.IO.Pipes;
using System.Threading;
using Helpers;

namespace TelegramRemoteControl
{
    public class Program
    {
        public static void Main(string[] args)
        {
            /*using (var pipe = new NamedPipeClientStream(".", "AimpRcPipe", PipeDirection.InOut))
            {
                pipe.Connect();

                using (var sw = new StreamWriter(pipe))
                {
                    while (true)
                    {
                        var text = Console.ReadLine();
                        sw.WriteLine(text);
                    }
                }
            }

            return;

            using (var brightnessController = new BrightnessController())
            {
                brightnessController.SetBrightness(90);
            }
            return;*/

            var thread = new Thread(() =>
            {
                var helper = new InitializationHelper();
                helper.Start(out var error, Console.WriteLine, Notify);
            });
            thread.Start();

            while (true)
            {
                Console.WriteLine("enter command:");
                var command = Console.ReadLine();

                switch (command)
                {
                    case "exit":
                        return;
                    case "restart":
                        thread.Abort();
                        thread = new Thread(() =>
                        {
                            var helper = new InitializationHelper();
                            helper.Start(out var error, Console.WriteLine, Notify);
                        });
                        thread.Start();
                        break;
                }
            }

            /*while (error == null)
            {
                var key = Console.ReadKey();

                if (key.Key.HasFlag(ConsoleKey.F1))
                {
                    helper.RestartServiceOverTime(Console.WriteLine, Notify, true);
                }

                if (key.Key.HasFlag(ConsoleKey.F4))
                {
                    helper.Stop(Console.WriteLine);
                    break;
                }
            }

            Console.WriteLine(error);*/
            Console.WriteLine("Closing app...");
            Thread.Sleep(7000);
        }

        private static Action<string> Notify
        {
            get
            {
                return x =>
                {
                    Console.Clear();
                    Console.WriteLine($"\n\nREMINDER\n{x}\n\n");
                };
            }
        }
    }
}
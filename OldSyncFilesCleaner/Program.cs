﻿using System;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;

namespace OldSyncFilesCleaner
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var dateLimit = DateTime.Now.AddDays(-int.Parse(ConfigurationManager.AppSettings["Days"]));

            Old(new[] { ConfigurationManager.AppSettings["OldFolder"] }, dateLimit);

            var newFolder = ConfigurationManager.AppSettings["Folder"];
            New(new[] { newFolder + "\\C", newFolder + "\\D", newFolder + "\\P", newFolder + "\\Samsung" }, dateLimit);
            
            Console.WriteLine("Finished.");
            for (int i = 10; i > 0; i--)
            {
                Console.WriteLine($"Closing after {i} minutes");
                Thread.Sleep(60 * 1000);
            }
        }

        private static void New(string[] foldersList, DateTime dateLimit)
        {
            foreach (var folder in foldersList)
            {
                string[] directoryNames;
                try
                {
                    directoryNames = Directory.GetDirectories(folder, "*", SearchOption.TopDirectoryOnly);
                }
                catch (Exception)
                {
                    continue;
                }

                foreach (var directoryName in directoryNames.Select(x => x?.Replace(folder + "\\", "")?.Substring(0, 10)))
                {
                    //2019-03-13
                    DateTime date;
                    if (DateTime.TryParseExact(directoryName, "yyyy-MM-dd", DateTimeFormatInfo.InvariantInfo, DateTimeStyles.None, out date) && date > dateLimit)
                    {
                        continue;
                    }

                    DateTime.TryParse(directoryName, out date);
                    try
                    {
                        var directoryInfo = new DirectoryInfo(directoryName);

                        directoryInfo.GetFiles("*", SearchOption.AllDirectories).ToList().ForEach(x => x.Delete());
                        directoryInfo.GetDirectories("*", SearchOption.AllDirectories).ToList().ForEach(x => x.Delete());
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine($"{directoryName} ...failed");
                        Console.WriteLine(ex.Message);
                    }
                }
            }
        }

        private static void Old(string[] foldersList, DateTime dateLimit)
        {
            foreach (var folder in foldersList)
            {
                string[] fileNames;
                string[] directoryNames;
                try
                {
                    fileNames = Directory.GetFiles(folder, "*", SearchOption.AllDirectories);
                    directoryNames = Directory.GetDirectories(folder, "*", SearchOption.AllDirectories);
                }
                catch (Exception)
                {
                    continue;
                }

                foreach (var fileName in fileNames)
                {
                    var startIndex = (fileName.LastIndexOf('.') == -1 ? fileName.Length - 1 : fileName.LastIndexOf('.')) - 17;
                    if (startIndex < 0)
                    {
                        Console.WriteLine($"{fileName} ...ignored");
                        continue;
                    }

                    var dateStr = fileName.Substring(startIndex, 10);
                    DateTime date;
                    if (DateTime.TryParseExact(dateStr, "yyyy-MM-dd", new DateTimeFormatInfo(), DateTimeStyles.None, out date) && date < dateLimit)
                    {
                        try
                        {
                            var fi = new FileInfo(fileName);
                            if (fi.IsReadOnly)
                                fi.Attributes = FileAttributes.Normal;


                            fi.Delete();
                            Console.WriteLine($"{fileName} ...deleted");
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine($"{fileName} ...failed {ex.Message}");
                        }
                    }
                }

                foreach (var directoryName in directoryNames)
                {
                    try
                    {
                        var directoryInfo = new DirectoryInfo(directoryName);
                        if (!directoryInfo.EnumerateDirectories().Any() && !directoryInfo.EnumerateFiles().Any())
                        {
                            directoryInfo.Delete();
                            Console.WriteLine($"{directoryName} ...deleted");
                        }
                    }
                    catch (Exception)
                    {
                        Console.WriteLine($"{directoryName} ...failed");
                    }
                }
            }  
        }
    }
}
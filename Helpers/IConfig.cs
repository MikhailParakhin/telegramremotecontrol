﻿using System.Collections.Generic;

namespace Helpers
{
    public interface IConfig
    {
        string BotToken { get; set; }

        Dictionary<string, long> RegisteredPCs { get; set; }
    }
}

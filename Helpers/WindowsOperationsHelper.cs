﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;

namespace Helpers
{
    public static class WindowsOperationsHelper
    {
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool AllowSetForegroundWindow(int dwProcessId);

        [DllImport("user32.dll")]
        private static extern int SetForegroundWindow(IntPtr hWnd);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        [Obsolete]
        private static void SendKeysToProcess(string processName, string keys, out bool isSuccess, out string response)
        {
            SendKeysToProcess(processName, keys, out isSuccess);

            if (isSuccess == false)
            {
                response = $"{processName} not running";
                return;
            }

            response = $"Pressing {keys} in {processName} window";
        }

        // https://stackoverflow.com/questions/3047375/simulating-key-press-c-sharp
        private static void SendKeysToProcess(string processName, string keys, out bool isSuccess)
        {
            var process = Process.GetProcessesByName(processName).FirstOrDefault();
            if (process == null)
            {
                isSuccess = false;
                return;
            }

            AllowSetForegroundWindow(process.Id);
            ShowWindow(process.MainWindowHandle, 9);
            SetForegroundWindow(process.MainWindowHandle);
            Console.WriteLine("Foreground set");
            Thread.Sleep(1000);

            SendKeys.SendWait(keys);
            // SendKeys.SendWait("^({PGUP})");

            Console.WriteLine("Key pressed");

            isSuccess = true;
        }

        public static Process ExecuteWindowsCommand(string exe, string command)
        {
            return Process.Start(exe, command);
        }

        public static Process Sleep()
        {
            //return ExecuteWindowsCommand("rundll32.exe", "/C powrprof.dll,SetSuspendState Standby");
            return ExecuteWindowsCommand("psshutdown.exe", " -d -t 0 -accepteula");
        }

        public static void Lock()
        {
            ExecuteWindowsCommand("rundll32.exe", "/C user32.dll, LockWorkStation");
        }

        public static void CloseApp(string appName)
        {
            ExecuteWindowsCommand("cmd.exe", $"/C taskkill /IM {appName}.exe >nul");
        }

        public static Process OffScreen()
        {
            return ExecuteWindowsCommand("cmd.exe", "/C START /MIN /WAIT C:\\Windows\\System32\\scrnsave.scr -s");
        }
    }
}

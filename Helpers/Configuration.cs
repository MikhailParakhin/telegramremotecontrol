using System.Collections.Generic;
using System.Linq;
using WindowsInput.Native;
using Telegram.Bot.Types.ReplyMarkups;

namespace Helpers
{
    public static class Configuration
    {
        public static readonly Dictionary<string, VirtualKeyCode> GlobalMusicHotkeys = new Dictionary<string, VirtualKeyCode>
        {
            {CommandNames.Next, VirtualKeyCode.MEDIA_NEXT_TRACK},
            {CommandNames.Prev, VirtualKeyCode.MEDIA_PREV_TRACK},
            {CommandNames.Stop, VirtualKeyCode.MEDIA_STOP},
            {CommandNames.Play, VirtualKeyCode.MEDIA_PLAY_PAUSE},
        };

        public static readonly IReplyMarkup CommandsListKeyboardMarkup = new ReplyKeyboardMarkup(new[]
        {
            new[]
            {
                new KeyboardButton(GetKeyText(CommandNames.Test)),
                new KeyboardButton(GetKeyText(CommandNames.Lock)),
                new KeyboardButton(GetKeyText(CommandNames.OffScreen))
            },
            new[]
            {
                new KeyboardButton(GetKeyText(CommandNames.Music)),
                new KeyboardButton(GetKeyText(CommandNames.Volume)),
                new KeyboardButton(GetKeyText(CommandNames.ChromeTwitch))
                //new KeyboardButton(GetKeyText(CommandNames.DotaAccept))
            },
            new[]
            {
                new KeyboardButton(GetKeyText(CommandNames.Sleep)),
                new KeyboardButton(GetKeyText(CommandNames.CloseApp)),
                new KeyboardButton(GetKeyText(CommandNames.Remind))
            },
        });

        public static readonly IReplyMarkup MusicKeyboardMarkup = new ReplyKeyboardMarkup(new[]
        {
            new[]
            {
                new KeyboardButton(GetKeyText(CommandNames.VolumeUp5)),
                new KeyboardButton(GetKeyText(CommandNames.Play)),
                new KeyboardButton(GetKeyText(CommandNames.VolumeDn5))
            },
            new[]
            {
                new KeyboardButton(GetKeyText(CommandNames.Prev)),
                new KeyboardButton(GetKeyText(CommandNames.Stop)),
                new KeyboardButton(GetKeyText(CommandNames.Next)),
            },
            new[]
            {
                new KeyboardButton(GetKeyText(CommandNames.List)),
            },
        });
        
        public static Command ReplyByCommand(string cmd, string replyParam, string customReply)
        {
            return AllDataByCmd(cmd, replyParam: replyParam, customReply: customReply);
        }

        public static string RunResultByCommand(string cmd, string customResponse = null)
        {
            return AllDataByCmd(cmd, customResponse: customResponse).RunResultText;
        }

        private static Command AllDataByCmd(string cmd, string customResponse = null, string replyParam = null, string customReply = null, string searchKeyText = null)
        {
            var paramKeyboardMarkup = replyParam == null ? new ReplyKeyboardRemove() : CommandsListKeyboardMarkup;
            var musicKeyCode = GlobalMusicHotkeys.ContainsKey(cmd ?? "") ? GlobalMusicHotkeys[cmd] : VirtualKeyCode.NONAME;

            var fullList = new []
            {
                new Command(CommandNames.Start, null, "I'm online!", CommandsListKeyboardMarkup, null),
                new Command(CommandNames.List, null, "Commands list:", CommandsListKeyboardMarkup, "\uD83D\uDCDC"), // scroll
                new Command(CommandNames.Test, $"{customResponse}", null, CommandsListKeyboardMarkup, null),
                new Command(CommandNames.Lock, "PC locked", null, CommandsListKeyboardMarkup, "\uD83D\uDD12"), // lock
                new Command(CommandNames.Sleep, $"{customResponse}", "Enter delay in minutes:", paramKeyboardMarkup, "\uD83D\uDCA4"), // zzz
                new Command(CommandNames.DotaAccept, "Pressing Ctrl+PgUp to accept game found via Dota.ahk", null, CommandsListKeyboardMarkup, null), // octopus \uD83D\uDC19
                new Command(CommandNames.ChromeTwitch, "Starting Dota stream", null, CommandsListKeyboardMarkup, null),
                new Command(CommandNames.CloseApp, $"{customResponse}", "Enter app name:", paramKeyboardMarkup, null), // X "\u274C"
                new Command(CommandNames.Volume, $"{customResponse}", $"{customReply}", paramKeyboardMarkup, "\uD83D\uDD08"), // speaker
                new Command(CommandNames.OffScreen, "Turning off the PC screen", null, CommandsListKeyboardMarkup, "\uD83C\uDF1A"), // moon
                new Command(CommandNames.Music, null, "Music commands:", MusicKeyboardMarkup, "\uD83C\uDFB6"), // note
                new Command(CommandNames.Prev, $"Pressing {musicKeyCode}", null, MusicKeyboardMarkup, "\u23EA"), // <<
                new Command(CommandNames.Next, $"Pressing {musicKeyCode}", null, MusicKeyboardMarkup, "\u23E9"), // >>
                new Command(CommandNames.Play, $"Pressing {musicKeyCode}", null, MusicKeyboardMarkup, "\u25B6"), // triangle
                new Command(CommandNames.Stop, $"Pressing {musicKeyCode}", null, MusicKeyboardMarkup, "\uD83D\uDD33"), // square
                new Command(CommandNames.VolumeUp5, $"Volume up 5", null, MusicKeyboardMarkup, "\uD83D\uDD0A"), // speaker with 3 waves
                new Command(CommandNames.VolumeDn5, $"Volume down 5", null, MusicKeyboardMarkup, "\uD83D\uDD09"), // speaker with 1 wave
                new Command(CommandNames.Remind, $"{customResponse}", "enter time and text HHmm TEXT:", paramKeyboardMarkup, "\u23F0"), // clock
            };


            var item = fullList.SingleOrDefault(x => x.Cmd == cmd);

            if (searchKeyText != null)
            {
                item = fullList.SingleOrDefault(x => x.KeyboardText == searchKeyText) ?? item;
            }

            if (CommandNames.RequiersParam(cmd) && replyParam != null)
            {
                item.ReplyText = null;
            }

            return item;
        }

        public static string GetKeyText(string cmd)
        {
            return AllDataByCmd(cmd).KeyboardText ?? cmd;
        }

        public static string GetCmdByKeyText(string keyText)
        {
            return AllDataByCmd(null, searchKeyText: keyText)?.Cmd ?? keyText;
        }

        public static class CommandNames
        {
            public const string List = "/list";
            public const string Start = "/start";
            public const string Test = "/test";
            public const string Lock = "/lock";
            public const string Sleep = "/sleep";
            public const string DotaAccept = "/dota_accept";
            public const string ChromeTwitch = "/chrome_twitch";
            public const string CloseApp = "/close_app";
            public const string OffScreen = "/offscreen";
            public const string Music = "/music";
            public const string Prev = "/prev";
            public const string Next = "/next";
            public const string Play = "/play";
            public const string Stop = "/stop";
            public const string Remind = "/remind";
            public const string Volume = "/volume";
            public const string VolumeUp5 = "/volumeup5";
            public const string VolumeDn5 = "/volumedn5";

            public static bool RequiersParam(string cmd)
            {
                return (cmd == Sleep || cmd == CloseApp || cmd == Remind || cmd == Volume);
            }
        }

        public class Command
        {
            public Command(string cmd, string runResultText, string replyText, IReplyMarkup keyReplyMarkup, string keyText)
            {
                Cmd = cmd;
                KeyboardText = keyText;
                RunResultText = runResultText;
                ReplyText = replyText;
                KeyboardMarkup = keyReplyMarkup;
            }

            public string Cmd { get; set; }
            public string KeyboardText { get; set; }
            public string RunResultText { get; set; }
            public string ReplyText { get; set; }
            public IReplyMarkup KeyboardMarkup { get; set; }
        }
    }
}
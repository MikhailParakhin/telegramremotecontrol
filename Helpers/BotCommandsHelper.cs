using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Pipes;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WindowsInput;
using WindowsInput.Native;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;

namespace Helpers
{
    public class BotCommandsHelper
    {
        public BotCommandsHelper(TelegramBotClient botClient, Dictionary<string, long> registeredPCs, Action<string> writeLine, Action<string> notify)
        {
            _notify = notify;
            _writeLine = writeLine;
            _botClient = botClient;

            _currentPcChat = registeredPCs[Environment.MachineName];
        }

        private static TelegramBotClient _botClient;
        private readonly long _currentPcChat;

        private readonly InputSimulator _inputSimulator = new InputSimulator();
        private readonly SoundHelper _soundHelper = new SoundHelper();

        private readonly Action<string> _writeLine;
        private readonly Action<string> _notify;
        private string _lastCommandStored;
        private int _messageOffset;
        private const int _baseHibernationTime = 30 * 60; // seconds till sleep
        private int _hibernationMode = _baseHibernationTime;

        public async Task<bool> TestConnection()
        {
            var result = await _botClient.TestApiAsync();

            return result;
        }

        public async Task<bool> Start(bool isRestart = false)
        {
            _writeLine("Starting... Bot ID " + _botClient.BotId);
            var updates = (await _botClient.GetUpdatesAsync(allowedUpdates: new[] { UpdateType.Message })).Where(x => x.Message.Chat.Id == _currentPcChat);
            _messageOffset = updates.LastOrDefault()?.Id ?? 0;
            //await _botClient.SendTextMessageAsync(_adminChatId, $"Service successfully started on {Environment.MachineName}");

            if (!isRestart)
            {
                await Bot_OnMessage(new Chat { Id = _currentPcChat }, Configuration.CommandNames.Start);
            }

            while (true)
            {
                await GetUpdates();
            }
        }

        private async Task<bool> GetUpdates()
        {
            var updates = (await _botClient.GetUpdatesAsync(_messageOffset + 1, allowedUpdates: new[] { UpdateType.Message }))
                .Where(x => x.Message.Chat.Id == _currentPcChat);

            foreach (var update in updates)
            {
                await Bot_OnMessage(update.Message.Chat, update.Message.Text);
                _messageOffset = update.Id;
                _writeLine("UPDATE: " + update.Message.Text);
                _hibernationMode = _baseHibernationTime; // refresh seconds till sleep
            }

            var sleep = _hibernationMode <= 0 ? 5000 : 1000;
            Thread.Sleep(sleep);
            _hibernationMode--;
            if (_hibernationMode == 0)
            {
                _writeLine("Entering hibernation mode.");
                await _botClient.SendTextMessageAsync(_currentPcChat, $"Entering hibernation mode.");
            }

            return true;
        }

        public Task<Message> SendTextMessage(string text)
        {
            return _botClient.SendTextMessageAsync(_currentPcChat, text);
        }

        public async Task Stop()
        {
            await SendTextMessage("I'm offline...");
            //await _botClient.SendTextMessageAsync(_adminChatId, $"Service stopped on {Environment.MachineName}");
            return;
        }

        [STAThread]
        private async Task Bot_OnMessage(Chat chat, string text)
        {
            if (string.IsNullOrWhiteSpace(text))
            {
                return;
            }

            string replyText = null;
            IReplyMarkup replyMarkup = new ReplyKeyboardRemove();
            var isSuccess = false;
            try
            {
                ParseCommandText(text, out var cmd, out var parameters);

                ReplyToCommand(cmd, parameters[0], out replyText, out replyMarkup);
                if (replyText != null)
                {
                    await _botClient.SendTextMessageAsync(chat, replyText, replyMarkup: replyMarkup);
                    return;
                }

                ExecuteCommand(cmd, parameters, out isSuccess, out replyText);
            }
            catch (Exception ex)
            {
                await _botClient.SendTextMessageAsync(chat, $"EXCEPTION:\n{text}\n{ex.Message}\n{ex.StackTrace}");
            }

            _writeLine($"Received a text message in chat {chat.Id}\n{text}");

            await _botClient.SendTextMessageAsync(chat, $"{(isSuccess ? "" : "FAILED:\n")}{replyText ?? text}", replyMarkup: replyMarkup);
            
        }

        private void ParseCommandText(string text, out string cmd, out string[] parameters)
        {
            var list = text.ToLower().Split(' ');
            cmd = list.Length > 0 ? list[0] : null;
            cmd = Configuration.GetCmdByKeyText(cmd);
            parameters = list.Length > 2 ? new[] { list[1], list[2] } : list.Length > 1 ? new[] { list[1], null } : new string[] { null, null };

            if (_lastCommandStored != null)
            {
                parameters[1] = parameters[0];
                parameters[0] = cmd;
                cmd = _lastCommandStored;
            }
        }

        private void ReplyToCommand(string cmd, string param, out string replyText, out IReplyMarkup replyMarkup)
        {
            var customReply = (cmd == Configuration.CommandNames.Volume) ? $"Current volume is {_soundHelper.GetCurrentVolume()}. Enter new:" : null;
            var reply = Configuration.ReplyByCommand(cmd, param, customReply);

            replyText = reply?.ReplyText;
            replyMarkup = reply?.KeyboardMarkup ?? Configuration.CommandsListKeyboardMarkup;

            if (Configuration.CommandNames.RequiersParam(cmd) && param == null)
            {
                _lastCommandStored = cmd;
            }
        }

        private void ExecuteCommand(string cmd, string[] parameters, out bool isSuccess, out string response)
        {
            var param1 = parameters[0];
            var param2 = parameters[1];

            if (param1 == Configuration.CommandNames.List)
            {
                ExecuteCommand(param1, new[] { "", "" }, out isSuccess, out response);
                return;
            }

            isSuccess = true;

            switch (cmd)
            {
                case Configuration.CommandNames.Test:
                    response = Configuration.RunResultByCommand(cmd, $"Hello there. Current time is {DateTime.Now}");
                    break;
                case Configuration.CommandNames.Lock:
                    response = Configuration.RunResultByCommand(cmd);
                    Run(WindowsOperationsHelper.Lock);
                    break;
                case Configuration.CommandNames.Sleep:
                    var timeout = param1.ToInt() ?? 1;
                    response = Configuration.RunResultByCommand(cmd, $"Sleep after {timeout} minutes");
                    Run(() =>
                    {
                        Thread.Sleep(5000);

                        using (var brightnessController = new BrightnessController())
                        {
                            //brightnessController.SetBrightness(10);
                        }

                        //_soundHelper.SmoothVolumeChange("Монитор", 40, timeout, () => { WindowsOperationsHelper.Sleep().WaitForExit(); });
                        _soundHelper.ChangeVolume(20);
                        Thread.Sleep(60 * 1000 * timeout);
                        WindowsOperationsHelper.Sleep();

                        Thread.Sleep(5000);
                        using (var brightnessController = new BrightnessController())
                        {
                            //brightnessController.SetBrightness(90);
                        }

                        // _soundHelper.SmoothVolumeChange("Монитор", 40, timeout, () => { WindowsOper ationsHelper.Sleep().WaitForExit(); });
                    });
                    break;
                case Configuration.CommandNames.DotaAccept: // aka detect game found and press enter once
                    response = Configuration.RunResultByCommand(cmd);
                    Run(() =>
                    {
                        WindowsOperationsHelper.ExecuteWindowsCommand("C:\\Code\\TelegramRemoteControl\\AHK\\AutoHotkeyU64.exe", "C:\\Code\\TelegramRemoteControl\\AHK\\dota.ahk");
                        Thread.Sleep(3000);
                        SendGlobalKeys(VirtualKeyCode.PRIOR, new[] { VirtualKeyCode.CONTROL });
                    });
                    break;
                case Configuration.CommandNames.ChromeTwitch:
                    response = Configuration.RunResultByCommand(cmd);
                    Run(() =>
                    {
                        WindowsOperationsHelper.ExecuteWindowsCommand("Services\\AutoHotkeyU64.exe", "Services\\chrome_twitch.ahk");
                        WindowsOperationsHelper.ExecuteWindowsCommand("C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe",
                            "/start-maximized /incognito /new-window /window-position=\"0, 0\" https://www.twitch.tv/directory/game/Dota%202");
                        Thread.Sleep(5000);
                        SendGlobalKeys(VirtualKeyCode.PRIOR, new[] { VirtualKeyCode.CONTROL });
                    });
                    break;
                case Configuration.CommandNames.CloseApp:
                    response = Configuration.RunResultByCommand($"Closing app {cmd}");
                    Run(() => WindowsOperationsHelper.CloseApp(param1));
                    break;
                case Configuration.CommandNames.Volume:
                    var newVolume = param1.ToInt() ?? _soundHelper.GetCurrentVolume();
                    response = Configuration.RunResultByCommand(cmd, $"Changing volume from {_soundHelper.GetCurrentVolume()} to {newVolume}");
                    Run(() => _soundHelper.ChangeVolume(newVolume));
                    break;
                case Configuration.CommandNames.OffScreen:
                    response = Configuration.RunResultByCommand(cmd);
                    Run(() => WindowsOperationsHelper.OffScreen());
                    break;
                case Configuration.CommandNames.Prev:
                case Configuration.CommandNames.Next:
                case Configuration.CommandNames.Play:
                case Configuration.CommandNames.Stop:
                    response = Configuration.RunResultByCommand(cmd);
                    SendGlobalKeys(Configuration.GlobalMusicHotkeys[cmd]);
                    break;
                case Configuration.CommandNames.VolumeUp5:
                    response = Configuration.RunResultByCommand(cmd);
                    Run(() => _soundHelper.ChangeVolume(_soundHelper.GetCurrentVolume() + 5));
                    break;
                case Configuration.CommandNames.VolumeDn5:
                    response = Configuration.RunResultByCommand(cmd);
                    Run(() => _soundHelper.ChangeVolume(_soundHelper.GetCurrentVolume() - 5));
                    break;
                case "songname":
                    throw new NotImplementedException();
                    using (var pipe = new NamedPipeClientStream(".", "AimpRcPipe", PipeDirection.Out))
                    {
                        pipe.Connect();

                        using (var sw = new StreamWriter(pipe))
                        {
                            sw.AutoFlush = true;
                            sw.WriteLine("Darude - Sandstorm");
                        }

                        pipe.Dispose();
                    }
                    response = "";
                    break;
                case Configuration.CommandNames.Remind:
                    throw new NotImplementedException();
                    var hour = param1?.Substring(0, 2).ToInt();
                    var minute = param1?.Substring(2, 2).ToInt();
                    if (hour == null || minute == null)
                    {
                        response = "Wrong param";
                        return;
                    }
                    var remindTime = DateTime.Today.AddHours(hour.Value).AddMinutes(minute.Value);
                    var now = DateTime.Now;
                    if (remindTime < now) //make tomorrow
                        remindTime = remindTime.AddDays(1);
                    response = Configuration.RunResultByCommand(cmd, $"{remindTime.Subtract(now).Hours} hours {remindTime.Subtract(now).Minutes} minutes");
                    Run(() =>
                    {
                        while (true)
                        {
                            if (DateTime.Now >= remindTime)
                            {
                                _writeLine("Reminder - " + DateTime.Now.ToShortTimeString());
                                _notify($"{DateTime.Now.ToShortTimeString()}\n{param2}");
                                return;
                            }

                            Thread.Sleep((60 - DateTime.Now.Second) * 1000); // wait till next 00 seconds
                        }
                    });
                    break;
                /*case Configuration.Commands.List:
                    response = Configuration.ResponseByCommand(cmd);
                    response = "lock - Lock PC\n" +
                                "sleep M - Put PC to sleep after M minutes\n" +
                                "dota_accept - Detect DotA2 game found and press enter\n" +
                                "close N - Close app N\n" +
                                "volume N - Set current sound device volume to N\n" +
                                "offscreen - Turn screen off\n" +
                                "music - play, stop, prev, next\n" +
                                "remind HHMM TEXT - remind TEXT at HH:MM\n";
                    break;*/
                default:
                    response = "Unknown command";
                    isSuccess = false;
                    break;
            }

            response = response ?? "EMPTY RESPONSE WHY";
            _lastCommandStored = null;
        }

        private void Run(Action action)
        {
            new Thread(() => { action(); }).Start();
        }

        private void SendGlobalKeys(VirtualKeyCode key, VirtualKeyCode[] modifier = null)
        {
            if (modifier == null)
            {
                _inputSimulator.Keyboard.KeyPress(key);
            }
            else
            {
                _inputSimulator.Keyboard.ModifiedKeyStroke(modifier, key);
            }
        }
    }
}
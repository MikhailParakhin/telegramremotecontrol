﻿using System.Configuration;
using MihaZupan;

namespace Helpers
{
    public static class ProxyConfig
    {
        public static HttpToSocks5Proxy Proxy => new HttpToSocks5Proxy(proxyUrl, proxyPort, proxyLogin, proxyPass) { ResolveHostnamesLocally = true };

        private static string proxyUrl => ConfigurationManager.AppSettings["ProxyUrl"] ?? "";
        private static int proxyPort => (ConfigurationManager.AppSettings["ProxyPort"] ?? "").ToInt().Value;
        private static string proxyLogin => ConfigurationManager.AppSettings["ProxyLogin"] ?? "";
        private static string proxyPass => ConfigurationManager.AppSettings["ProxyPass"] ?? "";
        private static string botToken => ConfigurationManager.AppSettings["BotToken"] ?? "";
    }
}

﻿namespace Helpers
{
    public static class CommonHelper
    {
        public static int? ToInt(this string value)
        {
            int i;
            if (int.TryParse(value, out i))
                return i;

            return null;
        }
    }
}

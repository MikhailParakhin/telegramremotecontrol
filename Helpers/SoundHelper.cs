﻿using System;
using System.Linq;
using System.Threading;
using AudioSwitcher.AudioApi;
using AudioSwitcher.AudioApi.CoreAudio;

namespace Helpers
{
    public class SoundHelper
    {
        private readonly CoreAudioController _audioController = new CoreAudioController();

        public int GetCurrentVolume()
        {
            return (int?)_audioController.DefaultPlaybackDevice?.Volume ?? -1;
        }

        public void ChangeVolume(int newVolume)
        {
            SmoothVolumeChange(_audioController.DefaultPlaybackDevice, newVolume);
        }

        public void SmoothVolumeChange(string targetDeviceName, int targetVolume, int minutes, Action runAction)
        {
            var devices = _audioController.GetDevices(DeviceType.Playback, DeviceState.Active);
            var targetDevice = devices.FirstOrDefault(x => x.Name.ToLower().Contains(targetDeviceName.ToLower()));
            targetDeviceName = targetDevice?.Name;
            if (targetDevice == null)
            {
                return;
            }

            var currentDevice = _audioController.GetDefaultDevice(DeviceType.Playback, Role.Multimedia);
            var currentDeviceName = currentDevice.Name;
            var currentVolume = (int)currentDevice.Volume;

            // Switch sound to target device
            SmoothVolumeChange(currentDevice, 0);
            targetDevice.SetVolumeAsync(0);
            targetDevice.SetAsDefault();
            SmoothVolumeChange(targetDevice, targetVolume);

            for (var i = 1; i <= minutes - 1; i++)
            {
                Console.WriteLine($"{i} minutes left");
                Thread.Sleep(60 * 1000);
            }

            if (minutes > 0)
            {
                Console.WriteLine($"1 minute left");
                Thread.Sleep(20 * 1000);
            }

            // mute target device and run action
            SmoothVolumeChange(targetDevice, 0);

            runAction();

            // restore state
            Thread.Sleep(5000);
            currentDevice = _audioController.GetDevices(DeviceType.Playback, DeviceState.Active).FirstOrDefault(x => x.Name == currentDeviceName);
            targetDevice = _audioController.GetDevices(DeviceType.Playback, DeviceState.Active).FirstOrDefault(x => x.Name == targetDeviceName);
            if (currentDevice == null || targetDevice == null)
            {
                return;
            }

            currentDevice.SetAsDefaultAsync();
            SmoothVolumeChange(currentDevice, currentVolume);
            SmoothVolumeChange(targetDevice, targetVolume, 0);
        }

        private static void SmoothVolumeChange(IDevice device, int targetVolume, int delay = 200, int step = 2)
        {
            if (device == null)
            {
                return;
            }

            step *= device.Volume < targetVolume ? 1 : -1;

            var startVolume = (int)device.Volume;
            for (var i = startVolume; i != Math.Abs(targetVolume); i += step)
            {
                device.SetVolumeAsync(i);
                Thread.Sleep(1 * delay);
            }
        }
    }
}
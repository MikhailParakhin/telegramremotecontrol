﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Telegram.Bot;

namespace Helpers
{
    public class InitializationHelper
    {
        private readonly Action<string> _writeLine;

        // ReSharper disable once InconsistentNaming
        private readonly BotCommandsHelper BotCommandsHelper;

        public InitializationHelper(Action<string> writeLine = null, Action<string> notify = null)
        {
            writeLine?.Invoke("Initializing...");
            throw new NotImplementedException();

            const long adminChatId = 239177594L;
            //var registeredPCs = new List<(long, string)> { { (adminChatId, "MIKHAIL-PC-NEW") }, { -389616374, "NASTYA-PC" }, { -379403549, "IT1239" } }; //TODO install everywhere
            BotCommandsHelper = new BotCommandsHelper(null, null, writeLine ?? Console.WriteLine, notify ?? Console.WriteLine);
        }

        // ReSharper disable once UnusedMember.Global
        public InitializationHelper(IConfig config, Action<string> writeLine = null, Action<string> notify = null)
        {
            _writeLine = writeLine ?? Console.WriteLine;
            _writeLine("Initializing...");

            var botClient = new TelegramBotClient(config.BotToken);
            BotCommandsHelper = new BotCommandsHelper(botClient, config.RegisteredPCs, _writeLine, notify ?? Console.WriteLine);
        }

        public void Start(out string error)
        {
            error = Start().Result;

            if (error != null)
                return;

            //RestartServiceOverTime(_writeLine, notify);
        }

        public Task<bool> Start2()
        {
            return BotCommandsHelper.Start();
        }

        private async Task<string> Start(bool isRestart = false)
        {
            if (TestConnection().Result == false)
            {
                await BotCommandsHelper.Stop();
                return "Connection failed";
            }
            else
            {
                await BotCommandsHelper.Start();

                _writeLine("Service started successfully");
                return "Service started successfully";
            }
        }

        public Task<bool> TestConnection()
        {
            return BotCommandsHelper.TestConnection();
        }

        public string SendMessage(string text)
        {
            var result = BotCommandsHelper.SendTextMessage(text);

            return result.Result.Text;
        }

        public async Task RestartServiceOverTime(Action<string> writeLine, Action<string> notify, bool runOnce = false)
        {
            throw new NotImplementedException();
            while (true)
            {
                await Task.Run(() => { Thread.Sleep(runOnce ? 0 : 2 * 60 * 60 * 1000); });

                //Stop(_writeLine);

                var error = Start(isRestart: true).Result;

                if (error != null)
                {
                    writeLine(error);
                    return;
                }

                writeLine("Service restarted successfully");

                if (runOnce)
                    return;
            }
        }

        public async Task<string> Stop(string textOutput = null)
        {
            _writeLine("\n\n\nService stopping...");

            await BotCommandsHelper.Stop();

            return "";
        }
    }
}

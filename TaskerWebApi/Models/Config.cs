﻿using System.Collections.Generic;
using System.Linq;
using Helpers;
using Microsoft.Extensions.Configuration;

namespace TaskerWebApi.Models
{
    public class Config : IConfig
    {
        public Config(IConfiguration configuration)
        {
            BotToken = configuration[nameof(BotToken)];

            RegisteredPCs = configuration.GetSection(nameof(RegisteredPCs))
                .AsEnumerable()
                .Select(x => new { PC = x.Key.Replace($"{nameof(RegisteredPCs)}", "").Replace(":", ""), ChatId = x.Value })
                .Where(x => !string.IsNullOrWhiteSpace(x.PC))
                .ToDictionary(x => x.PC, x => long.Parse(x.ChatId));
        }

        public string BotToken { get; set; }

        public Dictionary<string, long> RegisteredPCs { get; set; }
    }
}

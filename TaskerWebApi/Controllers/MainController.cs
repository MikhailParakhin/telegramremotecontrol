﻿using System.Threading.Tasks;
using Helpers;
using Microsoft.AspNetCore.Mvc;

namespace TaskerWebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class MainController : ControllerBase
    {
        private readonly InitializationHelper _initializationHelper;

        public MainController(InitializationHelper initializationHelper)
        {
            _initializationHelper = initializationHelper;
        }

        [HttpGet("Status")]
        public string GetStatus()
        {
            var testConnection = _initializationHelper.TestConnection();
            return testConnection.Result ? "OK" : "ERROR";
        }

        [HttpPost("TestMessage")]
        public string TestMessage(string message)
        {
            var result = _initializationHelper.SendMessage(message);

            return result;
        }

        [HttpPost("Start")]
        public string Start()
        {
            _initializationHelper.Start(out var error);

            return error;
        }

        [HttpPost("Stop")]
        public async Task<string> StopAsync()
        {
            return await _initializationHelper.Stop();
        }
    }
}

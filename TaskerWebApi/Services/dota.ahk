^pgup:: 
;MsgBox , , ,Ctrl+PgUp, 1 
SetTitleMatchMode, 2 ; approximate match
IfWinExist, Dota 2
{
	WinActivate, Dota 2
}
else
{
    MsgBox , , ,DotaNotFound, 1 
    return 
}
loop 
{ 
    CoordMode, Pixel, Screen 
    PixelGetColor, color, 875, 530 
	SplitBGRColor(color, Red, Green, Blue)
	; TODO ready check
    if ((Red = 60 and Green = 164 and Blue = 101) ;Hover-Lightened
	or (Red = 58 and Green = 91 and Blue = 76)) ;Darkened
    {
	    ;MsgBox , , ,WOAWOAWOAW, 1 
	    sendinput, {enter} 
		sleep 1000 
		ExitApp 
	}
	else
	{
	    ;MsgBox , , ,RGB-%Red%-%Green%-%Blue%, 1 		
	    sleep 10000 
    }
} 
return

^pgdn:: ; Ctrl+PgDn
;MouseGetPos, xpos, ypos 
;MsgBox, The cursor is at X%xpos% Y%ypos%. 
MsgBox , , ,Ctrl+PgDn - color and coors test, 1 
#Persistent 
SetTimer, WatchCursor, 100 
return 

WatchCursor: 
CoordMode, Mouse, Screen
CoordMode, Pixel, Screen
MouseGetPos, MouseX, MouseY 
PixelGetColor, color, %MouseX%, %MouseY% 
SplitBGRColor(color, Red, Green, Blue)
ToolTip, MouseX: %MouseX%`nMouseY: %MouseY%`ncolor: RGB-%Red%-%Green%-%Blue% 
return 

SplitBGRColor(BGRColor, ByRef Red, ByRef Green, ByRef Blue)
{
    Red := BGRColor & 0xFF
    Green := BGRColor >> 8 & 0xFF
    Blue := BGRColor >> 16 & 0xFF
}
﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Helpers;
using Microsoft.Extensions.Hosting;

namespace TaskerWebApi.Services
{
    public class TelegramBackgroundService : BackgroundService
    {
        private readonly InitializationHelper _initializationHelper;
        private bool _isStarted;

        public TelegramBackgroundService(InitializationHelper initializationHelper)
        {
            _initializationHelper = initializationHelper;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            Console.WriteLine($"TelegramBackgroundService is starting.");

            //stoppingToken.Register(() =>_logger.LogDebug($"TelegramBackgroundService is stopping."));

            while (!stoppingToken.IsCancellationRequested)
            {
                Console.WriteLine($"TelegramBackgroundService doing background work.");

                await Start();

                await Task.Delay(1000, stoppingToken);
            }

            Console.WriteLine($"TelegramBackgroundService is stopping.");
        }
        
        private async Task<string> Start(bool isRestart = false)
        {
            if (_initializationHelper.TestConnection().Result == false)
            {
                await _initializationHelper.Stop();
                return "Connection failed";
            }
            else
            {
                await StartThreadIfNotRunning();

                Console.WriteLine("Service started successfully");
                return "Service started successfully";
            }}


        private async Task StartThreadIfNotRunning()
        {
            if (!_isStarted)
            {
                _isStarted = true;
                await _initializationHelper.Start2();
            }
        }
    }
}

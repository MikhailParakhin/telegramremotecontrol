^pgup:: 
;MsgBox , , ,Ctrl+PgUp, 1 
loop 
{ 
    CoordMode, Pixel, Screen 
	MouseClick, left, 440, 640 ;
	sleep 5000 ;
	MouseClick, left, 440, 640 ;
	MouseClick, left, 440, 640 ;
    PixelGetColor, color, 440, 640 
	sleep 1000 
	ExitApp 
} 
return

^pgdn:: ; Ctrl+PgDn
;MouseGetPos, xpos, ypos 
;MsgBox, The cursor is at X%xpos% Y%ypos%. 
MsgBox , , ,Ctrl+PgDn - color and coors test, 1 
#Persistent 
SetTimer, WatchCursor, 100 
return 

WatchCursor: 
CoordMode, Mouse, Screen
CoordMode, Pixel, Screen
MouseGetPos, MouseX, MouseY 
PixelGetColor, color, %MouseX%, %MouseY% 
SplitBGRColor(color, Red, Green, Blue)
ToolTip, MouseX: %MouseX%`nMouseY: %MouseY%`ncolor: RGB-%Red%-%Green%-%Blue% 
return 

SplitBGRColor(BGRColor, ByRef Red, ByRef Green, ByRef Blue)
{
    Red := BGRColor & 0xFF
    Green := BGRColor >> 8 & 0xFF
    Blue := BGRColor >> 16 & 0xFF
}